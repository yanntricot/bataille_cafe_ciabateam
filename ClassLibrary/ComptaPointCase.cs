﻿namespace ClassLibrary
{
    public static class ComptaPointCase
    {
        /// <summary>
        /// Compte le nombre de points que le joueur peut obtenir en placant dans cette parcelle
        /// </summary>
        /// <param name="p_carte">La carte actuelle de la partie</param>
        /// <param name="p_indiceHorizontal">Coordonnée Horizontale actuelle de l'unité à vérifier</param>
        /// <param name="p_indiceVertical">Coordonnée Verticale actuelle de l'unité à vérifier</param>
        /// <param name="p_player">Référence vers le joueur pour lequel ont doit compter les points</param>
        /// <returns>Renvoie un Byte correspondant à combien de point le joueur gagne posant dans cette parcelle</returns>
        private static byte CalculPointTerritoire(Carte p_carte, Player p_player, byte p_indiceHorizontal, byte p_indiceVertical)
        {
            return p_carte.GetUnite(p_indiceHorizontal, p_indiceVertical).Parcelle.Owner == p_player //Verifie si le joueur à déjà le controle de la parcelle
                ? (byte)0 : p_carte.GetUnite(p_indiceHorizontal, p_indiceVertical).Parcelle.HaveControl(p_player) //S'il ne l'a pas alors vérifie s'il il gagne des points
                ? p_carte.GetUnite(p_indiceHorizontal, p_indiceVertical).Parcelle.SizeParcelle : (byte)0; //retourne les points gagnés
        }

        /// <summary>
        /// Permet de compter tout les points gagnés par un joueur sur la case selectionnée
        /// </summary>
        /// <param name="p_carte">La carte actuelle de la partie</param>
        /// <param name="p_indiceHorizontal">Coordonnée Horizontale actuelle de l'unité à vérifier</param>
        /// <param name="p_indiceVertical">Coordonnée Verticale actuelle de l'unité à vérifier</param>
        /// <param name="p_player">Référence vers le joueur pour lequel ont doit compter les points</param>
        /// <returns>Renvoie un byte définissant le nombre de point gagné par le joueur sur cette case</returns>
        private static byte Comptabilisation(Carte p_carte, Player p_player, byte p_indiceHorizontal, byte p_indiceVertical)
        {
            //initialisation de variable secondaire
            byte v_resultatCase = CalculPointTerritoire(p_carte, p_player, p_indiceHorizontal, p_indiceVertical);
            byte res = ComptaPointPartie.RechercheTailleTerrain(p_carte.Unites, p_indiceHorizontal, p_indiceVertical, ComptaPointPartie.InitialisationCarteBool());
            //Return new result
            return p_player.TaillePlusGrandeParcelle >= res ? v_resultatCase : (byte)(v_resultatCase + (byte)(res - p_player.TaillePlusGrandeParcelle));
        }

        /// <summary>
        /// Permet de compter tout les points pouvant être gagnés par un joueur sur la case selectionnée, et compare au score de l'adversaire les points gagnés.
        /// </summary>
        /// <param name="p_carte">La carte actuelle de la partie</param>
        /// <param name="p_indiceHorizontal">Coordonnée Horizontale actuelle de l'unité à vérifier</param>
        /// <param name="p_indiceVertical">Coordonnée Verticale actuelle de l'unité à vérifier</param>
        /// <param name="p_player">Référence vers le joueur pour lequel ont doit compter les points</param>
        /// <returns>Renvoie un byte définissant le nombre de points gagnés par le joueur sur cette case</returns>
        public static byte ComptabilisationPlacementFutur(Carte p_carte, Player p_player, byte p_indiceHorizontal, byte p_indiceVertical, byte p_ptsAdvDepart, Player p_player2)
        {
            if (!p_carte.GetUnite(p_indiceHorizontal, p_indiceVertical).IsUsuable) return 0;
            //Prépartation du terrain
            PlaceGraine(p_carte, p_player, p_indiceHorizontal, p_indiceVertical);
            byte v_resultatCase = Comptabilisation(p_carte, p_player, p_indiceHorizontal, p_indiceVertical);
            ComptaPointPartie.Comptabilisation(p_carte, p_player, p_player2);
            //Netoyage du terrain
            RetireGraine(p_carte, p_indiceHorizontal, p_indiceVertical);
            return (byte)(v_resultatCase + (p_ptsAdvDepart - p_player2.Points));
        }

        /// <summary>
        /// Permet de changer le controle de l'unité à la coordonnée fournie, en évitant d'actualiser l'état de la parcelle
        /// </summary>
        /// <param name="p_carte">La carte actuelle de la partie</param>
        /// <param name="p_indiceHorizontal">Coordonnée Horizontale actuelle de l'unité à vérifier</param>
        /// <param name="p_indiceVertical">Coordonnée Verticale actuelle de l'unité à vérifier</param>
        /// <param name="p_player">Référence vers le joueur pour lequel on doit compter les points</param>
        private static void PlaceGraine(Carte p_carte, Player p_player, byte p_indiceHorizontal, byte p_indiceVertical) =>
            p_carte.GetUnite(p_indiceHorizontal, p_indiceVertical).ControleLimite(p_player);

        /// <summary>
        /// Fonction annulant l'effet de PlaceGraine
        /// </summary>
        /// <param name="p_carte">La carte actuelle de la partie</param>
        /// <param name="p_indiceHorizontal">Coordonnée Horizontale actuelle de l'unité à vérifier</param>
        /// <param name="p_indiceVertical">Coordonnée Verticale actuelle de l'unité à vérifier</param>
        private static void RetireGraine(Carte p_carte, byte p_indiceHorizontal, byte p_indiceVertical) =>
            p_carte.GetUnite(p_indiceHorizontal, p_indiceVertical).ControleLibere();
    }
}