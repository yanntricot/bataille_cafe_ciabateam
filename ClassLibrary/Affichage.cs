﻿using System;

namespace ClassLibrary
{
    public static class Affichage
    {
        /// <summary>
        /// 'FCT AffichageParcelle' -
        ///
        /// Cette fonction permet l'affichage de la carte 10x10 d'unités en affichant
        /// l'ID de la parcelle dans laquelle est l'unité en [x,y].
        /// </summary>
        /// <param name="carte"> Instance de d'objet Carte contenant le tab </param>
        /// <remarks> L'affichage console s'effectue en 10x10 avec un tabulation </remarks>
        public static void AffichageParcelle(Carte carte)
        {
            Console.WriteLine("Affichage MaCarte > Parcelle - Unites");
            for (byte v_indexX = 0; v_indexX < 10; v_indexX++)
            {
                for (byte v_indexY = 0; v_indexY < 10; v_indexY++)
                    if (carte.GetUnite(v_indexX, v_indexY).Parcelle != null)
                        Console.Write(carte.GetUnite(v_indexX, v_indexY).Parcelle.NumeroParcelle + "\t");
                    else
                        Console.Write("null \t");
                Console.WriteLine();
            }
        }

        /******************************************************************************/

        /// <summary>
        /// 'FCT AffichageType' -
        ///
        /// Cette fonction permet l'affichage de la carte 10x10 d'unités en affichant
        /// le type de l'unité en [x,y].
        /// </summary>
        /// <param name="carte"> Instance de d'objet Carte contenant le tab </param>
        public static void AffichageType(Carte carte)
        {
            Console.WriteLine("Affichage MaCarte > Type - Unites");
            for (byte v_indexX = 0; v_indexX < 10; v_indexX++)
            {
                for (byte v_indexY = 0; v_indexY < 10; v_indexY++)
                    Console.Write(carte.GetUnite(v_indexX, v_indexY).Type + "\t");
                Console.WriteLine();
            }
        }

        /******************************************************************************/

        /// <summary>
        /// 'FCT AffichageOccupied' -
        ///
        /// Cette fonction permet l'affichage de la carte 10x10 d'unités en affichant
        /// si oui ou non l'unité en [x,y] est occupée.
        /// </summary>
        /// <param name="carte"> Instance de d'objet Carte contenant le tab </param>
        public static void AffichageOccupied(Carte carte)
        {
            Console.WriteLine("Affichage MaCarte > Occupied - Unites");
            for (byte v_indexX = 0; v_indexX < 10; v_indexX++)
            {
                for (byte v_indexY = 0; v_indexY < 10; v_indexY++)
                    Console.Write(carte.GetUnite(v_indexX, v_indexY).Occupied + "\t");
                Console.WriteLine();
            }
        }

        /******************************************************************************/

        /// <summary>
        /// 'FCT affichage_Owner' -
        ///
        /// Cette fonction permet l'affichage de la carte 10x10 d'unités en affichant
        /// l'ID du propriétaire de l'unité en [x,y].
        /// </summary>
        /// <param name="carte"> Instance de d'objet Carte contenant le tab </param>
        public static void affichage_Owner(Carte carte)
        {
            Console.WriteLine("Affichage MaCarte > Occupied - Unites");
            for (byte index_X = 0; index_X < 10; index_X++)
            {
                for (byte index_Y = 0; index_Y < 10; index_Y++)
                {
                    if (carte.GetUnite(index_X, index_Y).Owner == null)
                        Console.Write(0 + "\t");
                    else
                        Console.Write(carte.GetUnite(index_X, index_Y).Owner.Id + "\t");
                }
                Console.WriteLine();
            }
        }
    }
}