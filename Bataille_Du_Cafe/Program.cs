﻿using ClassLibrary;
using System;
using System.Net.Sockets;           // pour les sockets

namespace Bataille_Du_Cafe
{
    class Program
    {
        static void Main(string[] args)
        {
            // Initialisation d'une socket qui servira à l'insanciation d'un objet "Communication"
            Socket v_socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            // Instanciation d'un objet "Communication" avec une 'IP', un 'PORT' et une 'SOCKET'
            Communications v_com = new Communications("127.0.0.1", 1213, v_socket);

            // On récupère dans 'res_trame' la carte encodée ici en entiers
            string v_resTrame = v_com.ReceiveInfo(v_com.SocketPartie);

            // On instancie l'initialisation de la partie en lui passant en paramètre la trame récupérée 
            Partie v_init = new Partie(v_resTrame);

            // On récupère la carte alors initialisé dans une varaible de type "Carte"
            Carte v_carte = v_init.MaCarte;

            // On instancie deux joueurs pour la partie à venir
            Player v_joueur1 = new Player(1), v_joueur2 = new Player(2);

            // On lance la partie
            v_init.PremierTour(ref v_carte, ref v_com, ref v_joueur1, ref v_joueur2);
            // Permet de garder la console ouverte après exécution et de voir le score de fin de partie
            ComptaPointPartie.Comptabilisation(v_carte, v_joueur1, v_joueur2);
            Console.WriteLine("Resultat final : " + v_joueur1.Points + " a " + v_joueur2.Points);
            Console.ReadKey();
        }
    }
}
