﻿using System.Net.Sockets;           // pour les sockets
using System.Text;

namespace ClassLibrary
{
    public class Communications
    {
        // Attributs de la classe permettant la construction de celle-ci
        private string m_IP;

        private int m_port;
        public Socket SocketPartie { get; }

        /******************************************************************************/

        /// <summary>
        /// Constructeur par défaut de la classe Communications avec des paramètres
        /// paramètres de construction.
        /// </summary>
        /// <param name="p_IP">L'IP du serveur où nous souhaitons nous connecter</param>
        /// <param name="p_port">Le Port du serveur où nous souhaitons nous connecter</param>
        /// <param name="p_socket">Une socket servant de lien entre notre programme et le serveur</param>
        public Communications(string p_IP, int p_port, Socket p_socket)
        {
            m_IP = p_IP;
            m_port = p_port;
            SocketPartie = p_socket;

            SocketPartie.Connect(m_IP, m_port);
        }

        /******************************************************************************/

        /// <summary>
        /// 'FCT GetBytes' -
        ///
        /// Cette fonction permet de convertir une chaîne de caractère donnée en
        /// paramètre en un tableau de byte.
        /// </summary>
        /// <param name="p_str">Chaîne de caractère à envoyer</param>
        /// <returns>Retourne un tableau de byte</returns>
        private static byte[] GetBytes(string p_str)
        {
            byte[] v_bytes = new byte[p_str.Length * sizeof(char)];
            v_bytes = Encoding.ASCII.GetBytes(p_str);
            return v_bytes;
        }

        /******************************************************************************/

        /// <summary>
        /// 'FCT GetString' -
        ///
        /// Cette fonction permet de convertir un tableau de byte donnée en
        /// paramètre en une chaîne de caractère.
        /// </summary>
        /// <param name="p_bytes">Tableau de byte à décoder</param>
        /// <returns>Retourne une chaîne de caractère</returns>
        private static string GetString(byte[] p_bytes) => Encoding.ASCII.GetString(p_bytes);

        /******************************************************************************/

        /// <summary>
        /// 'FCT SendInfo' -
        ///
        /// Cette fonction permet d'envoyer une information au serveur lorsque
        /// l'on souhaite jouer un coup pour la partie.
        /// </summary>
        /// <param name="p_coup">Chaîne de caractère du coup à envoyer</param>
        /// <param name="p_socket">Socket servant de lien entre notre programme et le serveur</param>
        public void SendInfo(string p_coup, Socket p_socket) => p_socket.Send(GetBytes(p_coup));

        /******************************************************************************/

        /// <summary>
        /// 'FCT ReceiveInfo' -
        ///
        /// Cette fonction permet de recevoir une information du serveur lorsque
        /// l'on souhaite récupérer la carte ou le coup du serveur.
        /// </summary>
        /// <param name="p_socket">Socket servant de lien entre notre programme et le serveur</param>
        /// <returns>Retourne une chaîne de caractère</returns>
        public string ReceiveInfo(Socket p_socket)
        {
            byte[] v_buffer = new byte[512];

            p_socket.Receive(v_buffer);

            return GetString(v_buffer).Trim('\0').Trim('|');
        }
    }
}